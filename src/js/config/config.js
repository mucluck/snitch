const CONFIG = {
    SELECTOR: {
        MAIN_PREF: 'main',
        MAGIC: 'magic',
        TEXT: 'text',
        HREF: '#',
        BUTTON: 'btn',
        CLOSE: 'close',
        DESCRIPTOION: 'descr',
        HEADER: 'header',
        WRAP: 'wrap',
        TEXTURE: 'texture',
        WING: 'wing',
        CONTAINER: 'snitch-container',

        ROOT: 'body',
        MAIN: 'snitch-game',
        CSS: {
            TRANSFORM: 'transform',
            OPACITY: 'opacity'
        },
        SNITCH_GAME: 'snitch-game-snitch',
        SNITCH_BOX: 'snitch-game-box',
        START_GAME: 'snitch-game-start',
        DISMISS_GAME: 'snitch-game-dismiss',
        CLOSE_GAME: 'snitch-game-close',
        CLOSE_GAME_BOX: 'snitch-game-close-box',
        DISMISS: 'snitch-game-dismiss',
        CURSOR: {
            OPENED: 'snitch-game-cursor',
            CLOSED: 'snitch-game-cursor-closed'
        }
    },
    EVENTS: {
        CLICK: 'click',
        DOWN: 'mousedown',
        UP: 'mouseup'
    },
    MESSAGES: {
        en: {
            HEADER: 'Catch the Golden Snitch!',
            DESCRIPTION: 'Become the best Quidditch player!',
            START_BTN: 'I want to catch it!',
            DISMISS_BTN: 'A bit Later.',
            OVER: 'Good job!',
            CATCH: 'The snitch was caught in',
            TIME: 'seconds',
            REPLAY: 'Try again?',
            GAME_START: 'Snitch Game Has Been Started!'
        },
        ru: {
            HEADER: 'Поймай снитч!',
            DESCRIPTION: 'Стань лучшим игроком в квиддич!',
            START_BTN: 'Я поймаю его!',
            DISMISS_BTN: 'Чуть позже.',
            OVER: 'Ловец года!',
            CATCH: 'Снитч пойман за',
            TIME: 'секунд',
            REPLAY: 'Сыграем ещё?',
            GAME_START: 'Игра В Снитч Началась!'
        }
    },
    MAGIC: {
        MIN: 4,
        MAX: 20
    },
    GAME: {
        START: 'start',
        STOP: 'stop',
        WAIT: 'wait'
    },
    COLOR: ['#ffd740', '#fff'],
    STORAGE: {
        NAME: 'snitchPopupState',
        CLOSED: 'closed',
        OPENED: 'opened',
        SCORE: 'snitchGameScore',
        OPENED: 'opened',
    },
    POSITION: {
        x: 0,
        y: 0
    },
    SIZE: {
        w: 0,
        h: 0
    },
    SPEED: 300,
    SOCIAL: {
        en: {
            theme: {
                services: 'facebook,twitter',
                lang: 'en',
            },
            content: {
                url: document.origin,
                title: 'The golden snitch was caught in ',
                description: 'It`s time for you to seriously consider a career as a Seeker.',
            },
            contentByService: {
                twitter: {
                    hashtags: 'snitch,harrypotter,movavi,movaviblog'
                }
            }
        },
        ru: {
            theme: {
                services: 'facebook,vkontakte',
                lang: 'ru',
            },
            content: {
                url: document.origin,
                title: 'Золотой снитч пойман за ',
                description: 'Тебе пора всерьёз задуматься о карьере ловца.',
            }
        }
    }
};

module.exports = CONFIG;