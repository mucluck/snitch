import SnitchGameConfig from '../config/config';
import SnitchGamePopupStart from '../tpl/popup-start';
import SnitchGameCup from '../tpl/cup';
import SnitchGame from './game';

const SnitchPopup = (($) => {
    class SnitchPopup {
        constructor(locale, texture, delay) {
            this._locale = locale;
            this._delay = delay * 1000;
            this._config = SnitchGameConfig;
            this._container = SnitchGamePopupStart(this);
            this._root = $(this._config.SELECTOR.ROOT);
            this._cup = SnitchGameCup(parseInt(localStorage.getItem(this._config.STORAGE.SCORE)) > -1 ? localStorage.getItem(this._config.STORAGE.SCORE) : 0);
            this._game = new SnitchGame(this._locale, texture, this);
            this._state = localStorage.getItem(this._config.STORAGE.NAME);
        }

        init() {
            if (this._state !== this._config.STORAGE.CLOSED) {
                setTimeout(() => this.open(), this._delay)
            } else {
                this._root.append(this._cup);
            }
            this._container
                .on(this._config.EVENTS.CLICK, `#${this._config.SELECTOR.CLOSE_GAME}, #${this._config.SELECTOR.DISMISS}`, (e) => {
                    e.preventDefault();
                    this.close();
                    this._game.game(this._config.GAME.WAIT);
                })
                .on(this._config.EVENTS.CLICK, `#${this._config.SELECTOR.START_GAME}`, (e) => {
                    e.preventDefault();
                    this.close();
                    this._game.game(this._config.GAME.START);
                });
            this._cup.on(this._config.EVENTS.CLICK, (e) => {
                e.preventDefault();
                this.open();
            });
            return this;
        }
        open() {
            this._root.append(this._container);
            this._cup.remove();
        }
        wait() {
            this._cup = SnitchGameCup(parseInt(localStorage.getItem(this._config.STORAGE.SCORE)) > -1 ? localStorage.getItem(this._config.STORAGE.SCORE) : 0);
            this._root.append(this._cup.on(this._config.EVENTS.CLICK, (e) => {
                e.preventDefault();
                this.open();
            }));
            this._container.detach();
        }
        close() {
            this._state = this._config.STORAGE.CLOSED;
            this._container.detach();
            localStorage.setItem(this._config.STORAGE.NAME, this._config.STORAGE.CLOSED);
        }
    }

    return SnitchPopup;
})(jQuery);

module.exports = SnitchPopup;