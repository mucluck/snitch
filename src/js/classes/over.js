import SnitchGameConfig from '../config/config';
import SnitchGamePopupOver from '../tpl/popup-over';

const SnitchSnitchGamePopupOver = (($) => {
    class SnitchSnitchGamePopupOver {
        constructor(locale, score, game) {
            this._locale = locale;
            this._result = score;
            this._game = game;
            this._config = SnitchGameConfig;
            this._container = SnitchGamePopupOver(this);
            this.init();
        }
        init() {
            this._container.on(this._config.EVENTS.CLICK, `#${this._config.SELECTOR.CLOSE_GAME}`, (e) => {
                e.preventDefault();
                this.close();
            });
            this._container.on(this._config.EVENTS.CLICK, `#${this._config.SELECTOR.START_GAME}`, (e) => {
                e.preventDefault();
                this.restart();
            });
        }
        open() {
            $(this._config.SELECTOR.ROOT).append(this._container);
            return this;
        }
        close() {
            this._container.remove(); 
            this._game.game(this._config.GAME.WAIT);
        }
        restart() {
            this._container.remove();
            this._game.game(this._config.GAME.START);
        }
    }

    return SnitchSnitchGamePopupOver;
})(jQuery);

module.exports = SnitchSnitchGamePopupOver;