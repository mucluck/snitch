import SnitchGameConfig from '../config/config';
import SnitchGameSnitch from '../tpl/snitch';
import SnitchSnitchGamePopupOver from './over';

const SnitchGame = (($) => {
    class SnitchGame {
        constructor(locale, texture, window) {
            this._inited = false;
            this._locale = locale;
            this._texture = texture;
            this._window = window;
            this._config = SnitchGameConfig;
            this._box = SnitchGameSnitch(this);
            this._over = {};
            this._snitch = this._box.find(`#${this._config.SELECTOR.SNITCH_BOX}`);
            this._size = this._config.SIZE;
            this._position = this._config.POSITION;
            this._score = localStorage.getItem(this._config.STORAGE.SCORE) && parseInt(localStorage.getItem(this._config.STORAGE.SCORE)) > -1 ? localStorage.getItem(this._config.STORAGE.SCORE) : -1;
            this._loop = 0;
            this._time = 0;
            this._opacity = 0;
            this._screensot = {};
        }

        game(state) {
            switch (state) {
                case 'start':
                    this._time = this.time();
                    if (!this._inited) {
                        $(this._config.SELECTOR.ROOT).append(this._box.addClass(this._config.SELECTOR.CURSOR.OPENED));
                        this._inited = true;
                    }
                    this._box
                        .removeClass(this._config.SELECTOR.CURSOR.CLOSED)
                        .on(this._config.EVENTS.UP, () => this._box.removeClass(this._config.SELECTOR.CURSOR.CLOSED))
                        .on(this._config.EVENTS.DOWN, () => this._box.addClass(this._config.SELECTOR.CURSOR.CLOSED))
                        .on(this._config.EVENTS.DOWN, `#${this._config.SELECTOR.SNITCH_GAME}`, (e) => {
                            e.preventDefault();
                            this.game(this._config.GAME.STOP);
                        })
                        .off(this._config.EVENTS.CLICK, `#${this._config.SELECTOR.CLOSE_GAME_BOX}`)
                        .on(this._config.EVENTS.CLICK, `#${this._config.SELECTOR.CLOSE_GAME_BOX}`, (e) => {
                            e.preventDefault();
                            this._over instanceof SnitchSnitchGamePopupOver ? this._over.close() : this.game(this._config.GAME.WAIT);
                        });
                    this._size = {
                        w: this._snitch.outerWidth(),
                        h: this._snitch.outerHeight()
                    };
                    this.loop();
                    console.info(this._config.MESSAGES[this._locale].GAME_START);
                    $('main, footer').fadeOut();
                    break;
                case 'stop':
                    let currentScore = parseFloat(((this.time() - this._time) / 1000).toFixed(1));
                    this._config.SOCIAL[this._locale].content.title = `${this._config.SOCIAL[this._locale].content.title} ${currentScore} ${this._config.MESSAGES[this._locale].TIME}!`;
                    this._over = new SnitchSnitchGamePopupOver(this._locale, currentScore, this).open();
                    this._box.off(`${this._config.EVENTS.DOWN} ${this._config.EVENTS.UP}`);
                    this._score = this._score === -1 || this._score > currentScore ? currentScore : this._score;
                    this.hideSnitch();
                    localStorage.setItem(this._config.STORAGE.SCORE, this._score);
                    clearInterval(this._loop);
                    break;
                case 'wait':
                    this._window.wait();
                    this.close();
                    clearInterval(this._loop);
                    $('main, footer').fadeIn();
                    break;
            }
            return this;
        }

        close() {
            this._inited = false;
            this._box.remove();
        }

        hideSnitch() {
            this._snitch.css(this._config.SELECTOR.CSS.TRANSFORM, `translate3d(${this._position.x}px, ${this._position.y}px, 0) scale(0)`);
        }

        loop() {
            this._loop = setInterval(() => {
                let scale = Math.random();
                this._position.x = this.randomInteger($(window).innerWidth() - this._size.w, 0);
                this._position.y = this.randomInteger($(window).innerHeight() - this._size.h, 0);
                this._snitch.css(this._config.SELECTOR.CSS.TRANSFORM, `translate3d(${this._position.x}px, ${this._position.y}px, 0) scale(${scale})`);
            }, this._config.SPEED);
            setTimeout(() => this._snitch.css(this._config.SELECTOR.CSS.OPACITY, 1), this._config.SPEED * 2);
        }

        time() {
            return new Date().getTime();
        }

        randomInteger(min, max) {
            return Math.round(min - 0.5 + Math.random() * (max - min + 1));
        }
    }

    return SnitchGame;
})(jQuery);

module.exports = SnitchGame;