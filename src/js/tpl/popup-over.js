function PopupOver(instance) {
    return $(`<div class="snitch-game-popup snitch-game-popup_over">
                    <a id="${instance._config.SELECTOR.CLOSE_GAME}" href="#" class="snitch-game-popup__close" data-owox="{&quot;label&quot;: &quot;snitch_game&quot;, &quot;content&quot;: &quot;again_popup&quot;}"></a>
                    <h3 class="snitch-game-popup__header snitch-game-popup__header_over">${instance._config.MESSAGES[instance._locale].OVER}</h3>
                    <div class="snitch-game-popup__cup-wrap">
                        <div class="snitch-game-popup__magic snitch-game-popup__magic_over" ></div>
                        <div class="snitch-game-popup__magic snitch-game-popup__magic_over" ></div>
                        <div class="snitch-game-popup__magic snitch-game-popup__magic_over" ></div>
                        <div class="snitch-game-popup__magic snitch-game-popup__magic_over" ></div>
                        <div class="snitch-game-popup__magic snitch-game-popup__magic_over" ></div>
                        <div class="snitch-game-popup__magic snitch-game-popup__magic_over" ></div>
                        <div class="snitch-game-popup__cup-item">
                            <img src="//img.movavi.com/img.movavi.17/images/snitch-games/cup.png" alt="Snitch Cup">
                        </div>
                    </div>
                    <div class="snitch-game-popup__result-wrap">
                        <span class="snitch-game-popup__descr snitch-game-popup__descr_over">${instance._config.MESSAGES[instance._locale].CATCH}</span>
                        <span class="snitch-game-popup__result">${parseFloat(instance._result) > -1 ? instance._result : 0}</span>
                        <span class="snitch-game-popup__descr snitch-game-popup__descr_over">${instance._config.MESSAGES[instance._locale].TIME}</span>
                    </div>
                    <div class="snitch-game-popup__btn-wrap snitch-game-popup__btn-wrap_over">
                        <a id="${instance._config.SELECTOR.START_GAME}" href="#" class="snitch-game-popup__btn snitch-game-popup__btn_over" data-owox="{&quot;label&quot;: &quot;snitch_game&quot;, &quot;content&quot;: &quot;try_again&quot;}">
                            <span class="snitch-game-popup__text snitch-game-popup__text_over">
                                ${instance._config.MESSAGES[instance._locale].REPLAY}
                            </span>
                        </a>
                        <div id="snitch-game-social" class="snitch-game-popup__social">
                            <script type="text/javascript">Ya.share2('snitch-game-social', ${JSON.stringify(instance._config.SOCIAL[instance._locale])});</script>
                        </div>
                    </div>
                </div>`);
}

module.exports = PopupOver;