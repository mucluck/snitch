function Snitch(instance) {
    return $(`<div class="snitch-game snitch-game-cursor">
                <a id="${instance._config.SELECTOR.CLOSE_GAME_BOX}" class="snitch-game-popup__close snitch-game-popup__close_box" href="#" data-owox="{&quot;label&quot;: &quot;snitch_game&quot;, &quot;content&quot;: &quot;game_popup&quot;}"></a>
                <div id="${instance._config.SELECTOR.SNITCH_BOX}" class="snitch-game__box">
                    <a id="snitch-game-snitch" href="#" class="snitch-game__fly">
                        <img id="snitch-game__img" src="${instance._texture}" class="snitch-game__img" alt="Snitch Game"/>
                    </a>
                </div>
            </div>`);
}

module.exports = Snitch;