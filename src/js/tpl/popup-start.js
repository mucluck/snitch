function PopupStart(instance) {
    return $(`<div class="snitch-game-popup snitch-game-popup_main">
                <div class="snitch-game-popup__magic" style="top: 170px; left: 12px; transform: rotate(231deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 49px; left: 112px; transform: rotate(156deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 150px; left: 216px; transform: rotate(177deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 99px; left: 69px; transform: rotate(208deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 24px; left: 99px; transform: rotate(312deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 65px; left: 378px; transform: rotate(81deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 19px; left: 228px; transform: rotate(322deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 135px; left: 91px; transform: rotate(246deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 89px; left: 397px; transform: rotate(104deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 42px; left: 267px; transform: rotate(152deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 100px; left: 285px; transform: rotate(326deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 128px; left: 91px; transform: rotate(55deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 98px; left: 40px; transform: rotate(254deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 23px; left: 91px; transform: rotate(175deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 77px; left: 127px; transform: rotate(175deg); border-color: rgb(255, 215, 64);"></div>
                <div class="snitch-game-popup__magic" style="top: 11px; left: 30px; transform: rotate(209deg); border-color: rgb(255, 255, 255);"></div>
                <div class="snitch-game-popup__magic" style="top: 181px; left: 190px; transform: rotate(70deg); border-color: rgb(255, 215, 64);"></div>
                <h3 class="snitch-game-popup__header">${instance._config.MESSAGES[instance._locale].HEADER}</h3>
                <span class="snitch-game-popup__descr">${instance._config.MESSAGES[instance._locale].DESCRIPTION}</span>
                <a id="${instance._config.SELECTOR.CLOSE_GAME}" class="snitch-game-popup__close" href="#" data-owox="{&quot;label&quot;: &quot;snitch_game&quot;, &quot;content&quot;: &quot;catch_popup&quot;}"></a>
                <div class="snitch-game-popup__snitch-container">
                    <svg class="snitch-game-popup__wing" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 318.77 524.49">
                        <defs>
                            <style>
                                .wing-1 {
                                    fill: #ddd;
                                    stroke: #201600;
                                    stroke-miterlimit: 10;
                                    stroke-width: 4px;
                                }

                                .cls-2 {
                                    fill: #fff;
                                }
                            </style>
                        </defs>
                        <g>
                            <g>
                                <path d="M2.74,484.33s113-66,164-161,70-224,74-270,0-52,0-52,40,45,41,86,0,55,0,55,24,0,22,36-11,58-11,58,25-4,24,16-10,61-19,69a167.41,167.41,0,0,1-19,14.33s12,17.92,0,29.8-19,26-25,44.43-23,29-32,31.73-22,2.05-22,2.05,2,32.66-16,40.66-24,8.39-24,8.39-25,25.11-53,28.86S22.74,518.33,2.74,484.33Z"
                                    class="wing-1"></path>
                                <path d="M289.44,234.18s9-22,11-58-22-36-22-36,1-14,0-55c-.81-33.43-19.85-54.41-29.7-66.61,8.46,34.51,12.6,67,18.62,88.12,10,35-14,41-14,41s18,16,33,28-18,58-18,58,19,20,26,26-2,30-7,41-31,18-31,18,17,26,6,34.23-21,17.77-34,48.77-48,29-48,29-2,31-22,37-26,15-68,30c-25.78,9.21-58.28-5.13-81.62-12.78,23.76,26.28,69.35,37.91,94.7,34.52,28-3.75,53-28.86,53-28.86s6-.39,24-8.39,16-40.67,16-40.67,13,.67,22-2,26-13.29,32-31.73,13-32.56,25-44.43,0-29.8,0-29.8a167.41,167.41,0,0,0,19-14.33c9-8,18-49,19-69S289.44,234.18,289.44,234.18Z"
                                    class="cls-2"></path>
                            </g>
                        </g>
                    </svg>
                    <svg class="snitch-game-popup__wing" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 318.77 524.49">
                        <defs>
                            <style>
                                .wing-1 {
                                    fill: #ddd;
                                    stroke: #201600;
                                    stroke-miterlimit: 10;
                                    stroke-width: 4px;
                                }

                                .cls-2 {
                                    fill: #fff;
                                }
                            </style>
                        </defs>
                        <g>
                            <g>
                                <path d="M2.74,484.33s113-66,164-161,70-224,74-270,0-52,0-52,40,45,41,86,0,55,0,55,24,0,22,36-11,58-11,58,25-4,24,16-10,61-19,69a167.41,167.41,0,0,1-19,14.33s12,17.92,0,29.8-19,26-25,44.43-23,29-32,31.73-22,2.05-22,2.05,2,32.66-16,40.66-24,8.39-24,8.39-25,25.11-53,28.86S22.74,518.33,2.74,484.33Z"
                                    class="wing-1"></path>
                                <path d="M289.44,234.18s9-22,11-58-22-36-22-36,1-14,0-55c-.81-33.43-19.85-54.41-29.7-66.61,8.46,34.51,12.6,67,18.62,88.12,10,35-14,41-14,41s18,16,33,28-18,58-18,58,19,20,26,26-2,30-7,41-31,18-31,18,17,26,6,34.23-21,17.77-34,48.77-48,29-48,29-2,31-22,37-26,15-68,30c-25.78,9.21-58.28-5.13-81.62-12.78,23.76,26.28,69.35,37.91,94.7,34.52,28-3.75,53-28.86,53-28.86s6-.39,24-8.39,16-40.67,16-40.67,13,.67,22-2,26-13.29,32-31.73,13-32.56,25-44.43,0-29.8,0-29.8a167.41,167.41,0,0,0,19-14.33c9-8,18-49,19-69S289.44,234.18,289.44,234.18Z"
                                    class="cls-2"></path>
                            </g>
                        </g>
                    </svg>
                    <svg class="snitch-game-popup__texture" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 367.73 367.56">
                        <defs>
                            <style>
                                .cls-1 {
                                    fill: #ffd740;
                                    stroke: #201600;
                                }

                                .cls-1,
                                .cls-3 {
                                    stroke-miterlimit: 10;
                                    stroke-width: 4px;
                                }

                                .cls-2 {
                                    fill: #fff;
                                }

                                .cls-3 {
                                    fill: none;
                                    stroke: #000;
                                }
                            </style>
                        </defs>
                        <g>
                            <g>
                                <circle cx="183.78" cy="183.78" r="181.78" class="cls-1"></circle>
                                <path d="M37.64,184.7c0-93.29,73-169.83,166-177.66q-7.78-.66-15.75-.67C87.52,6.37,6.14,86.21,6.14,184.7S87.52,363,187.92,363q8,0,15.75-.67C110.65,354.53,37.64,278,37.64,184.7Z"
                                    class="cls-2"></path>
                                <path d="M3.13,163.43s46.44,15.78,94.7,28.85c24.27,6.58,52,13.08,64.42,34.57,11,19.06.59,63.33-30.35,84.33S64,320.54,64,320.54"
                                    class="cls-3"></path>
                                <path d="M132.83,226.85S48.17,219.53,2,192.28" class="cls-3"></path>
                                <path d="M137.35,264.29S52.69,257,6.52,229.73" class="cls-3"></path>
                                <path d="M124.93,294s-69.83-4.63-99.45-20.49" class="cls-3"></path>
                                <path d="M365.58,163.43s-46.43,15.78-94.7,28.85c-24.27,6.58-52,13.08-64.41,34.57-11,19.06-.6,63.33,30.34,84.33s67.87,9.36,67.87,9.36"
                                    class="cls-3"></path>
                                <path d="M235.88,226.85s84.66-7.32,130.83-34.57" class="cls-3"></path>
                                <path d="M231.36,264.29S316,257,362.19,229.73" class="cls-3"></path>
                                <path d="M243.79,294s69.83-4.63,99.44-20.49" class="cls-3"></path>
                                <path d="M165.93,84.6s-16.68,43.29-53.68,53.64-93.68-27-93.68-27" class="cls-3"></path>
                                <path d="M201.63,86.64s16.68,43.29,53.68,53.65,93.69-27,93.69-27" class="cls-3"></path>
                                <path d="M108.48,18.29S129,40.2,128.83,54" class="cls-3"></path>
                                <path d="M153.74,4.49S160.83,34,161.83,46" class="cls-3"></path>
                                <path d="M255.55,18.29S235.08,40.2,235.21,54" class="cls-3"></path>
                                <path d="M210.3,4.49S203.21,34,202.21,46" class="cls-3"></path>
                            </g>
                        </g>
                    </svg>
                </div>
                <div class="snitch-game-popup__btn-wrap">
                    <a id="${instance._config.SELECTOR.START_GAME}" class="snitch-game-popup__btn" href="#" data-owox="{&quot;label&quot;: &quot;snitch_game&quot;, &quot;content&quot;: &quot;catch&quot;}">
                        <span class="snitch-game-popup__text">${instance._config.MESSAGES[instance._locale].START_BTN}</span>
                    </a>
                    <a id="${instance._config.SELECTOR.DISMISS_GAME}" class="snitch-game-popup__btn" href="#" data-owox="{&quot;label&quot;: &quot;snitch_game&quot;, &quot;content&quot;: &quot;later&quot;}">
                        <span class="snitch-game-popup__text">${instance._config.MESSAGES[instance._locale].DISMISS_BTN}</span>
                    </a>
                </div>
            </div>`);
}

module.exports = PopupStart;